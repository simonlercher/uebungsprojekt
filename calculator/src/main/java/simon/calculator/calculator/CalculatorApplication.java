package simon.calculator.calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.swing.*;
import java.util.Scanner;

@SpringBootApplication
public class CalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculatorApplication.class, args);
		Scanner scan = new Scanner(System.in);

		System.out.println("The World's Finest Calculator!");

		double dividend = 0;
		double divisor = 1;
		double quotient = 1;


		System.out.println("Bitte geben Sie ihr gewuenschtes Dividend ein!: ");
		dividend = scan.nextDouble();


			System.out.println("Bitte geben Sie ihren gewuenschten Divisor ein!: ");
			divisor = scan.nextDouble();

			if (divisor == 0) {
				System.out.println("Eine division durch 0 ist nicht moeglich!\nDer Divisor wurde auf 1 gesetzt!");
				divisor = 1;
			}


		quotient = dividend / divisor;

		System.out.println("Das Ergebnis lautet: "+quotient);
		System.out.println("Danke dass Sie meinen Calculator verwendet haben. :) ");


	}

}
